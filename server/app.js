const express = require("express");
const GraphqlHTTP = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");

const { graphqlHTTP } = GraphqlHTTP;

const app = express();

mongoose.connect(
	"mongodb+srv://subendra:arsenal123@cluster0.pxwip.mongodb.net/graphqllearn?retryWrites=true&w=majority"
);
mongoose.connection.once("open", () => {
	console.log("connected!");
});

app.use(
	"/graphql",
	graphqlHTTP({
		schema,
		graphiql: true,
	})
);

app.listen(4000, () => {
	console.log("listening on port 4000");
});
